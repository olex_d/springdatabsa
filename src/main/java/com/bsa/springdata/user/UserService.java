package com.bsa.springdata.user;

import com.bsa.springdata.office.OfficeRepository;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

	private final UserRepository userRepository;
	private final OfficeRepository officeRepository;
	private final TeamRepository teamRepository;

	public UserService(UserRepository userRepository, OfficeRepository officeRepository, TeamRepository teamRepository) {
		this.userRepository = userRepository;
		this.officeRepository = officeRepository;
		this.teamRepository = teamRepository;
	}

	public Optional<UUID> createUser(CreateUserDto userDto) {
		try {
			var office = officeRepository.getOne(userDto.getOfficeId());
			var team = teamRepository.getOne(userDto.getTeamId());

			var user = User.fromDto(userDto, office, team);
			var result = userRepository.save(user);
			return Optional.of(result.getId());
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	public Optional<UserDto> getUserById(UUID id) {
		return userRepository.findById(id).map(UserDto::fromEntity);
	}

	public List<UserDto> getUsers() {
		var users = userRepository.findAll();
		return UserDto.fromEntitiesList(users);
	}

	public List<UserDto> findByLastName(String lastName, int page, int size) {
		var lastNameSort = Sort
				.sort(User.class)
				.by(User::getLastName)
				.ascending();
		var paging = PageRequest.of(page, size, lastNameSort);

		var users = userRepository.findByLastName(lastName, paging);
		return UserDto.fromEntitiesList(users);
	}

	public List<UserDto> findByCity(String city) {
		var users = userRepository.findByOfficeCityOrderByLastNameAsc(city);
		return UserDto.fromEntitiesList(users);
	}

	public List<UserDto> findByExperience(int experience) {
		var users = userRepository.findByExperienceGreaterThanEqualOrderByExperienceDesc(experience);
		return UserDto.fromEntitiesList(users);
	}

	public List<UserDto> findByRoomAndCity(String city, String room) {
		var lastNameSort = Sort
				.sort(User.class)
				.by(User::getLastName)
				.ascending();

		var users = userRepository.findByOfficeCityAndTeamRoom(city, room, lastNameSort);
		return UserDto.fromEntitiesList(users);
	}

	public int deleteByExperience(int experience) {
		return userRepository.deleteByExperienceLessThan(experience);
	}

}
