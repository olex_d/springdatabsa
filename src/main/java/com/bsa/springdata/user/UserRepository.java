package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

	@Query("SELECT u FROM User u WHERE lower(u.lastName) LIKE lower(concat('%', ?1,'%'))")
	List<User> findByLastName(String lastName, Pageable lastNameSort);

	List<User> findByOfficeCityOrderByLastNameAsc(String city);

	List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

	List<User> findByOfficeCityAndTeamRoom(String city, String room, Sort lastNameSort);

	@Modifying
	int deleteByExperienceLessThan(int experience);

}
