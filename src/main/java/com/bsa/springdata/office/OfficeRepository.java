package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OfficeRepository extends JpaRepository<Office, UUID> {

	Optional<OfficeDto> findByAddress(String address);

	@Query(" SELECT DISTINCT o FROM Office o " +
			"JOIN o.users u " +
			"JOIN u.team t " +
			"JOIN t.technology tech " +
			"WHERE tech.name = ?1 ")
	List<Office> getByTechnology(String technology);

	@Modifying
	@Query(" UPDATE Office o SET o.address = ?2 " +
			"WHERE o.id IN ( " +
			"SELECT u.office.id FROM User u " +
			") AND o.address = ?1 ")
	void updateAddress(String oldAddress, String newAddress);

}
