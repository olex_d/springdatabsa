package com.bsa.springdata.office;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {

	private final OfficeRepository officeRepository;

	public OfficeService(OfficeRepository officeRepository) {
		this.officeRepository = officeRepository;
	}

	public List<OfficeDto> getByTechnology(String technology) {
		var offices = officeRepository.getByTechnology(technology);
		return OfficeDto.fromEntitiesList(offices);
	}

	public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
		officeRepository.updateAddress(oldAddress, newAddress);
		return officeRepository.findByAddress(newAddress);
	}

}
