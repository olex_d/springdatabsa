package com.bsa.springdata.project.dto;

import com.bsa.springdata.project.Project;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@Builder
public class ProjectDto {
	private final UUID id;
	private final String name;
	private final String description;

	public static ProjectDto fromEntity(Project project) {
		return ProjectDto
				.builder()
				.id(project.getId())
				.name(project.getName())
				.description(project.getDescription())
				.build();
	}

	public static List<ProjectDto> fromEntitiesList(List<Project> projects) {
		return projects
				.stream()
				.map(ProjectDto::fromEntity)
				.collect(Collectors.toList());
	}
}
