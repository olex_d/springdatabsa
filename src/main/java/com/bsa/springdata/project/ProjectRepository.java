package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<Project, UUID> {

	@Query(" SELECT prj FROM Project prj " +
			"JOIN prj.teams teams " +
			"JOIN teams.users users " +
			"JOIN teams.technology tech " +
			"WHERE tech.name = ?1 " +
			"GROUP BY prj.id " +
			"ORDER BY COUNT(users.id) DESC ")
	List<Project> findTopTeamsByTechnology(String technology, Pageable pageable);

	@Query(" SELECT prj FROM Project prj " +
			"JOIN prj.teams teams " +
			"JOIN teams.users users " +
			"GROUP BY prj.id, prj.name " +
			"ORDER BY COUNT(DISTINCT teams.id) DESC, COUNT(DISTINCT users.id) DESC, prj.name DESC ")
	List<Optional<Project>> findTheBiggest(Pageable pageable);

	@Query(value =
			"SELECT " +
				"projects.name," +
				"COUNT(DISTINCT(t.id)) teamsNumber, " +
				"COUNT(DISTINCT(u.id)) developersNumber, " +
				"STRING_AGG(DISTINCT(tech.name), ',') technologies " +
			"FROM projects " +
				"JOIN teams t on projects.id = t.project_id " +
				"JOIN users u ON t.id = u.team_id " +
				"JOIN technologies tech on t.technology_id = tech.id " +
			"GROUP BY projects.id, projects.name " +
			"ORDER BY projects.name "
			, nativeQuery = true)
	List<ProjectSummaryDto> getSummary();

	@Query(" SELECT COUNT(DISTINCT prj.id) FROM Project prj " +
			"JOIN prj.teams team " +
			"JOIN team.users user " +
			"JOIN user.roles role " +
			"WHERE role.name = ?1 " +
			"GROUP BY role.id")
	int getCountWithRole(String roleName);

}
