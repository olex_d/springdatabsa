package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.TeamService;
import com.bsa.springdata.technology.TechnologyService;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectService {

	private final ProjectRepository projectRepository;
	private final TechnologyService technologyService;
	private final TeamService teamService;

	public ProjectService(ProjectRepository projectRepository,
	                      TechnologyService technologyService, TeamService teamService) {
		this.projectRepository = projectRepository;
		this.technologyService = technologyService;
		this.teamService = teamService;
	}

	public List<ProjectDto> findTop5ByTechnology(String technology) {
		var top5paging = PageRequest.of(0, 5);
		var projects = projectRepository.findTopTeamsByTechnology(technology, top5paging);
		return ProjectDto.fromEntitiesList(projects);
	}

	public Optional<ProjectDto> findTheBiggest() {
		var top1paging = PageRequest.of(0, 1);

		var biggestProject = projectRepository.findTheBiggest(top1paging);
		if (biggestProject.isEmpty()) return Optional.empty();

		return biggestProject.get(0).map(ProjectDto::fromEntity);
	}

	public List<ProjectSummaryDto> getSummary() {
		return projectRepository.getSummary();
	}

	public int getCountWithRole(String role) {
		return projectRepository.getCountWithRole(role);
	}

	public UUID createWithTeamAndTechnology(CreateProjectRequestDto projectDto) {
		var technology = technologyService.createTechForProject(projectDto);
		var teams = teamService.createTeamsForProject(projectDto, technology);

		var newProject = Project.fromDto(projectDto, teams);
		var savedProject = projectRepository.save(newProject);

		return savedProject.getId();
	}

}
