package com.bsa.springdata.role;

import org.springframework.stereotype.Service;

@Service
public class RoleService {

	private final RoleRepository roleRepository;

	public RoleService(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	public void deleteRole(String roleCode) {
		roleRepository.deleteUnusedRole(roleCode);
	}

}
