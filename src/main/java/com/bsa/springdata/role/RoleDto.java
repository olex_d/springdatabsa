package com.bsa.springdata.role;

import lombok.Data;

import java.util.UUID;

@Data
public class RoleDto {
	private final UUID id;
	private final String name;
	private final String code;
}
