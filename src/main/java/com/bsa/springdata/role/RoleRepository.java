package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {

	@Modifying
	@Query("DELETE FROM Role r WHERE r.code = ?1 AND r.users IS EMPTY ")
	void deleteUnusedRole(String roleCode);

}
