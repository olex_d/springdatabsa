package com.bsa.springdata.team;

import com.bsa.springdata.project.Project;
import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.technology.Technology;
import com.bsa.springdata.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "teams")
public class Team {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", nullable = false, updatable = false)
	private UUID id;

	@Column
	private String name;

	@Column
	private String room;

	@Column
	private String area;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "project_id")
	private Project project;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "team")
	private List<User> users;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "technology_id")
	private Technology technology;

	public static Team fromProjectDto(CreateProjectRequestDto projectDto, Technology technology) {
		return Team.builder()
				.name(projectDto.getTeamName())
				.area(projectDto.getTeamArea())
				.room(projectDto.getTeamRoom())
				.technology(technology)
				.build();
	}

}
