package com.bsa.springdata.team;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.technology.Technology;
import com.bsa.springdata.technology.TechnologyRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class TeamService {

	private final TeamRepository teamRepository;
	private final TechnologyRepository technologyRepository;

	public TeamService(TeamRepository teamRepository, TechnologyRepository technologyRepository) {
		this.teamRepository = teamRepository;
		this.technologyRepository = technologyRepository;
	}

	public List<Team> createTeamsForProject(CreateProjectRequestDto projectDto, Technology technology) {
		var teamForProject = Team.fromProjectDto(projectDto, technology);

		var dbTeam = teamRepository.findByName(teamForProject.getName());
		if (dbTeam.isEmpty()) teamForProject = teamRepository.save(teamForProject);

		return Collections.singletonList(teamForProject);
	}

	public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
		var newTechnology = technologyRepository.findByName(newTechnologyName);
		if (newTechnology.isEmpty()) return;

		var oldTeams = teamRepository.findByTechnologyAndDevsNumber(devsNumber, oldTechnologyName);
		for (var team : oldTeams) {
			team.setTechnology(newTechnology.get());
			teamRepository.save(team);
		}
	}

	public void normalizeName(String name) {
		teamRepository.normalizeName(name);
	}

}
