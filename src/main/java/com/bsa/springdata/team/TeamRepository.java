package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

	Optional<Team> findByName(String name);

	int countByTechnologyName(String newTechnology);

	@Query("SELECT t FROM Team t where t.technology.name = ?2 AND t.users.size < ?1 ")
	List<Team> findByTechnologyAndDevsNumber(int devsNumber, String technologyName);

	@Modifying
	@Query(value =
			"UPDATE teams SET name = CONCAT(teams.name, '_', p.name, '_', t.name) " +
			"FROM projects p, technologies t " +
			"WHERE p.id = teams.project_id AND t.id = teams.technology_id " +
			"AND teams.name = ?1 "
			, nativeQuery = true)
	void normalizeName(String name);

}
