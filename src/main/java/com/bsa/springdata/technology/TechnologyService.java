package com.bsa.springdata.technology;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import org.springframework.stereotype.Service;

@Service
public class TechnologyService {

	private final TechnologyRepository technologyRepository;

	public TechnologyService(TechnologyRepository technologyRepository) {
		this.technologyRepository = technologyRepository;
	}

	public Technology createTechForProject(CreateProjectRequestDto projectDto) {
		var techForProject = Technology.fromProjectDto(projectDto);

		var dbTech = technologyRepository.findByName(techForProject.getName());
		if (dbTech.isEmpty()) techForProject = technologyRepository.save(techForProject);

		return techForProject;
	}

}
