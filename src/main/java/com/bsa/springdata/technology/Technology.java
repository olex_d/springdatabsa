package com.bsa.springdata.technology;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "technologies")
public class Technology {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", nullable = false, updatable = false)
	private UUID id;

	@Column
	private String name;

	@Column
	private String description;

	@Column
	private String link;

	public static Technology fromProjectDto(CreateProjectRequestDto projectDto) {
		return Technology.builder()
				.name(projectDto.getTech())
				.link(projectDto.getTechLink())
				.description(projectDto.getTechDescription())
				.build();
	}

}
