package com.bsa.springdata.technology;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TechnologyRepository extends JpaRepository<Technology, UUID> {
	Optional<Technology> findByName(String name);
}
